﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using HW_database.Models;

#nullable disable

namespace HW_database
{
    public partial class SberbankContext : DbContext
    {
        public SberbankContext()
        {
        }

        public SberbankContext(DbContextOptions<SberbankContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Operation> Operations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=sberbank;Username=postgres;Password=123");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("account");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('account_id_inc'::regclass)");

                entity.Property(e => e.Ammount).HasColumnName("ammount");

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("frg_client_id");
            });

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("client");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('client_id_inc'::regclass)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("name");

                entity.Property(e => e.Passport).HasColumnName("passport");
            });

            modelBuilder.Entity<Operation>(entity =>
            {
                entity.ToTable("operation");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('operation_id_inc'::regclass)");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.OperDate)
                    .HasColumnType("date")
                    .HasColumnName("oper_date");

                entity.Property(e => e.Sum).HasColumnName("sum");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Operations)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("frg_account_id");
            });

            modelBuilder.HasSequence("account_id_inc");

            modelBuilder.HasSequence("client_id_inc");

            modelBuilder.HasSequence("operation_id_inc");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
