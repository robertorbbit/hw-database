﻿using HW_database.Models;
using System;
using System.Linq;

namespace HW_database
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type V to view tables, type table name to insert data (client or account or operation)");
            var command = Console.ReadLine().ToLower();
            if(command == "v") {
                using (SberbankContext sberbankDb = new SberbankContext())
                {
                    var clients = sberbankDb.Clients.ToList();
                    var accounts = sberbankDb.Accounts.ToList();
                    var operations = sberbankDb.Operations.ToList();

                    Console.WriteLine("Clients list(first table):");
                    foreach (var client in clients)
                    {
                        Console.WriteLine($"Id:{client.Id} Name: {client.Name} Passport: {client.Passport}");
                    }

                    Console.WriteLine("Accounts list(second table):");
                    foreach (var account in accounts)
                    {
                        Console.WriteLine($"Id:{account.Id} Ammoount: {account.Ammount} ClientId: {account.ClientId}");
                    }

                    Console.WriteLine("Operations list(third table):");
                    foreach (var operation in operations)
                    {
                        Console.WriteLine($"Id:{operation.Id} Date: {operation.OperDate} Sum: {operation.Sum} AccountId: {operation.AccountId}");
                    }

                    Console.WriteLine("Operations list(denormalize):");
                    foreach (var operation in operations)
                    {
                        Console.WriteLine($"Id:{operation.Id} Date: {operation.OperDate} Sum: {operation.Sum} Account Ammount: {operation.Account.Ammount} Client Name: {operation.Account.Client.Name}");
                    }
                }
            }
            else if (command == "client")
            {
                Console.WriteLine("Type client name");
                var name = Console.ReadLine();
                Console.WriteLine("Type client passport number");
                var passport = long.Parse(Console.ReadLine());

                using (SberbankContext sberbankDb = new SberbankContext())
                {
                    Client client = new Client { Name = name, Passport = passport };
                    sberbankDb.Clients.Add(client);
                    sberbankDb.SaveChanges();
                }
            }
            else if (command == "account")
            {
                Console.WriteLine("Type account ammount");
                var ammount = decimal.Parse(Console.ReadLine());
                Console.WriteLine("Type client Id");
                var clientId = long.Parse(Console.ReadLine());

                using (SberbankContext sberbankDb = new SberbankContext())
                {
                    Account account = new Account { Ammount = ammount, ClientId = clientId };
                    sberbankDb.Accounts.Add(account);
                    sberbankDb.SaveChanges();
                }
            }
            else if (command == "operation")
            {
                Console.WriteLine("Type operation sum");
                var sum = decimal.Parse(Console.ReadLine());
                Console.WriteLine("Type account Id");
                var accountId = long.Parse(Console.ReadLine());

                using (SberbankContext sberbankDb = new SberbankContext())
                {
                    Operation operation = new Operation { Sum = sum, AccountId = accountId, OperDate = DateTime.Now };
                    sberbankDb.Operations.Add(operation);
                    sberbankDb.SaveChanges();
                }
            }
        }

    }
}
