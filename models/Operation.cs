﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HW_database.Models
{
    public class Operation
    {
        public long Id { get; set; }
        public long AccountId { get; set; }
        public decimal Sum { get; set; }
        public DateTime OperDate { get; set; }

        public virtual Account Account { get; set; }
    }
}
