﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HW_database.Models
{
    public class Client
    {
        public Client()
        {
            Accounts = new HashSet<Account>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public long Passport { get; set; }

        public virtual ICollection<Account> Accounts { get; set; }
    }
}
