﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HW_database.Models
{
    public class Account
    {
        public Account()
        {
            Operations = new HashSet<Operation>();
        }

        public long Id { get; set; }
        public decimal? Ammount { get; set; }
        public long ClientId { get; set; }

        public virtual Client Client { get; set; }
        public virtual ICollection<Operation> Operations { get; set; }
    }
}
