CREATE TABLE IF NOT EXISTS public.client
(
    id bigint NOT NULL DEFAULT nextval('client_id_inc'::regclass),
    name character varying(255) NOT NULL,
    passport bigint NOT NULL,
    CONSTRAINT client_pkey PRIMARY KEY (id)
)
