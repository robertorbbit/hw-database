CREATE TABLE IF NOT EXISTS public.account
(
    id bigint NOT NULL DEFAULT nextval('account_id_inc'::regclass),
    ammount numeric,
    client_id bigint NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT frg_client_id FOREIGN KEY (client_id)
        REFERENCES public.client (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);