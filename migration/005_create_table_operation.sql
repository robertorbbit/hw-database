CREATE TABLE IF NOT EXISTS public.operation
(
    id bigint NOT NULL DEFAULT nextval('operation_id_inc'::regclass),
    account_id bigint NOT NULL,
    sum numeric NOT NULL,
    oper_date date NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT frg_account_id FOREIGN KEY (account_id)
        REFERENCES public.account (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
        NOT VALID
);